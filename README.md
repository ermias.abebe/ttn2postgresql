[TOC]

# Introduction

This is a simple script that will subscribe to The Things Network v3 (via MQTT) and write the payload values received to a PostgreSQL database.

It can handle multiple devices, managed via a config file.
The script subscribes via MQTT and creates a table per device, then writes the payload data as it comes in.

It provides support for the [TimescaleDB](https://www.timescale.com/), an extension built to optimise PostgreSQL for time-series data.

# TimescaleDB

If you want to use the TimescaleDB extension, it needs to be installed and enabled by following the [how-to guide](https://docs.timescale.com/timescaledb/latest/how-to-guides/install-timescaledb/self-hosted/).

If you don't just set `hypertables: False` in the config file.

The advantage of TimescaleDB is a huge improvement in performance and functionality for series-data.
Basically, we can cram all of our readings for a particular device into the same row, against the same time-stamp, without the need for any JOINs and without the risk of it bogging down our DB when it gets large.
Hypertables are partitioned into "chunks" and interlinked by a column timestamp to optimise them for time-series operations.
We also get some extra time-based [querying functions](https://docs.timescale.com/timescaledb/latest/how-to-guides/query-data/select/#select-commands) to make data visualisation and manipulation more powerful.

For a comparison of the performance of TimescaleDB vs vanilla PostgreSQL, check [here](https://docs.timescale.com/timescaledb/latest/overview/how-does-it-compare/timescaledb-vs-postgres/).

# Configuration

### Configuration File

The configuration file path is specified with a CLI argument.

```bash
usage: ttn2postgresql.py [-h] [-c CONFIG]
```

```bash
example:

python3 ttn2postgresql.py -c /etc/ttn2postgresql/config.yaml
python3 ttn2postgresql.py --config /etc/ttn2postgresql/config.yaml
```


### The Things Network

This section requires an API key (referred to as the password here, since that's what MQTT calls it), which can be specific to the app or global to your account.

To get an API key, follow [this](https://www.thethingsindustries.com/docs/integrations/mqtt/#creating-an-api-key) guide.

The server address and port can be found on the same page. You should use port 8883 for TLS support. I haven't tested the non-TLS port.

```yaml
ttn:
  app_id: "gps-tracker-sit123"
  password: "NNSXS.BLAH5467GRDGD4TF3446AW34ET.ODFHR5F7RDGLLVMI2O76DX6VG75SDF3F4WSILZ2CT456JNW"
  server: "au1.cloud.thethings.network"
  port: 8883
```

### PostgreSQL

This is fairly straightforward, aside from the hypertables flag.

Hypertables are a TimescaleDB feature, you can read more about them [here](https://docs.timescale.com/timescaledb/latest/how-to-guides/hypertables/#hypertables). This is what enables the tables to be partitioned into "chunks", which provides us with all the performance improvements that TimescaleDB offers.

The database and user must exist, with the correct access configured in your `pg_hba.conf` file if your are authenticating externally.

```yaml
postgres:
  username: "ttn"
  password: "secure-password!"
  host: "localhost"
  port: "5432"
  database: "ttn"
  hypertables: True
```

### Devices

Devices are added here and must match the device ID's in the console.
As mentioned above, a table is created for each device.
The columns specified in the config below will be created dynamically for each device table on first run.
If you screw this up, drop the table and start again - it won't re-create the table if it already exists.

The data types must match the generic types specified in SQLAlchemy's documentation [here](https://docs.sqlalchemy.org/en/14/core/type_basics.html#generic-types).

```yaml
devices:
  gps-tracker0:
    columns:
      analog_in_1: Float
      altitude: Float
      latitude: Float
      longitude: Float
  gps-tracker1:
    columns:
      analog_in_1: Float
      altitude: Float
      latitude: Float
      longitude: Float
```

# Running

### Systemd

To run the script as a service with systemd, create a user:

```bash
$ sudo useradd -r -d /opt/ttn2postgresql ttn
```

Clone the repo and move it to an appropriate directory:

```bash
$ git clone https://gitlab.com/shed909/ttn2postgresql
```

`sudo mv ttn2postgresql /opt/`

Install requirements:

```bash
$ cd /opt/ttn2postgresql
```

```bash
 $ pip3 install -r requirements.txt
 ```

NOTE:
If you get an issue while installing psycopg2, try install the following:

```bash
$ sudo apt-get install libpq-dev python-dev
```

Create a config file:

`sudo vim /etc/ttn2postgresql/config.yaml`

```yaml
ttn:
  app_id: "gps-tracker"
  password: "NNSXS.BLAHYKHWAKGWEJYSJ6EUTTJYA8EUVHCZLDQ5LDI.37FGIYSNATSKLT9EEJMU9682U3R63DQEFOIPYRWSYLD2WTF492UY"
  server: "au1.cloud.thethings.network"
  port: 8883

postgres:
  username: "ttn"
  password: "secure-password!"
  host: "localhost"
  port: "5432"
  database: "ttn"
  hypertables: True

devices:
  gps-tracker0:
    columns:
      analog_in_1: Float
      altitude: Float
      latitude: Float
      longitude: Float
  gps-tracker1:
    columns:
      analog_in_1: Float
      altitude: Float
      latitude: Float
      longitude: Float
```

Create a service file:

`sudo vim /etc/systemd/system/ttn2postgresql.service`

```bash
[Service]
Type=simple
User=ttn
ExecStart=/usr/bin/python3 /opt/ttn2postgresql/ttn2postgresql.py -c /etc/ttn2postgresql/config.yaml
Restart=always

[Install]
WantedBy=multi-user.target
```

Enable and start the service:

```bash
$ sudo systemctl enable ttn2postgresql
```
```bash
$ sudo systemctl start ttn2postgresql
```

### Docker

See the example [Dockerfile](Dockerfile), which uses the official Python3 image to dockerize the script.

A docker-compose example of running ttn2postgresql with a TimescaleDB container is also available in the [docker-compose.yaml](docker-compose.yaml) file.

# Troubleshooting

To troubleshoot issues, set the logging level to DEBUG via the provided CLI argument.

Example run command:

```bash
$ /usr/bin/python3 /opt/ttn2postgresql/ttn2postgresql.py -c /etc/ttn2postgresql/config.yaml -l DEBUG
```
