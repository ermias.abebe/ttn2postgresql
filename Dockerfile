FROM python:3.9

ARG TZ=Australia/Melbourne

WORKDIR /app

COPY ttn2postgresql.py .
COPY config.yaml .
COPY requirements.txt .

RUN pip3 install -r requirements.txt
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

CMD ["ttn2postgresql.py", "-l", "INFO"]
ENTRYPOINT ["python"]
