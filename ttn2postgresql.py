#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sqlalchemy import Column, event, DDL, create_engine, func
from sqlalchemy import (BigInteger, Boolean, Date, DateTime, Enum, Float,
                        Integer, Interval, LargeBinary, Numeric, PickleType,
                        SmallInteger, String, Text, Time, Unicode, UnicodeText)
from sqlalchemy.orm import Session, declarative_base
from logging import getLogger, basicConfig, ERROR, WARNING, INFO, DEBUG
from argparse import ArgumentParser, RawDescriptionHelpFormatter
import paho.mqtt.client as mqtt
import json
import yaml


DESCRIPTION = '''
A configurable daemon script, built to consume messages from The Things Network
via MQTT and write the payload values recieved to a PostgreSQL DB.
'''

EXAMPLE = '''
example:

python3 ttn2postgresql.py -c /etc/ttn2postgresql/config.yaml
python3 ttn2postgresql.py --config /etc/ttn2postgresql/config.yaml
'''

###############################################################################
#                          Helper functions live here                         #
###############################################################################


def parse_args(description, example):
    '''
    Function parse cli arguments.
    '''
    description = '''
    A configurable daemon script, built to consume
    messages from The Things Network via MQTT and write the
    payload values recieved to a PostgreSQL DB.'''

    parser = ArgumentParser(description=description,
                            epilog=example,
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('-c', '--config',
                        required=False,
                        default='config.yaml',
                        help='full path to the config file')
    parser.add_argument('-l', '--logging',
                        required=False,
                        default='INFO',
                        help='log output level')

    return parser.parse_args()


def get_yaml(path):
    '''
    Helper function to get yaml file contents.
    '''
    with open(path) as yaml_file:
        data = yaml.safe_load(yaml_file)

    return data


def flatten_dict(in_dict):
    '''
    Helper function to flatten a dictionary.
    CayenneLPP often places things like GPS in a nested structure.
    '''
    items = []

    for k, v in in_dict.items():
        if isinstance(v, dict):
            items.extend(flatten_dict(v).items())
        else:
            items.append((k, v))

    return dict(items)


def build_table(name, cols, hypertables=False):
    '''
    Function to create a Device table dynamically.
    Takes a name and a dict of columns (column name, data type).
    Hypertables can also be specified as True or False to configure this on
    table init.
    '''
    # use declaritive_base as out base class
    Base = declarative_base(class_registry=dict())

    # create our Device class from Base
    class Device(Base):
        __tablename__ = name

        # 'id' is required, but we set it's col name to be 'datetime'
        # TODO: we probably want an option to use the time from the message,
        #       instead of creating it at insert with now() function
        id = Column('datetime', DateTime(timezone=True),
                    default=func.now(), primary_key=True)

    def update_cols(model, cols):
        '''
        Method to update the colums of this Device table using dict from
        device columns specified in the config file
        '''
        for col, dtype in cols.items():
            setattr(model, col, Column(col, eval(dtype),
                                       primary_key=False, nullable=True))

    # run the update_cols method
    update_cols(Device, cols)

    # add a listener to the Device class so that on init, the DDL to configure
    # hypertables is ran.
    if hypertables and not getattr(Device, '_has_hypertable_listener', False):
        ddl = f"SELECT create_hypertable('{Device.__tablename__}', 'datetime');"
        event.listen(
            Device.__table__,
            'after_create',
            DDL(ddl)
        )
        Device._has_hypertable_listener = True

    return Device


def insert_row(values, device_id):
    '''
    Function to insert row to the table correlating to the device_id.
    '''
    # create a row object
    row = device_tables[device_id](**values)
    # insert the row with a session
    with Session(engine) as session:
        session.add(row)
        session.commit()
    # log some info for debugging
    db_logger.debug(f'Row inserted to {device_id} table with values: {values}')

###############################################################################


###############################################################################
#                   Callback functions for paho-mqtt live here                #
###############################################################################


def on_connect(client, obj, flags, rc):
    '''
    This runs once connected to the MQTT broker and subscribes to each topic.
    Topics are constructed of the app_id's and devices defined in the specified
    config file.
    '''
    # subscribe to all devices in config.yaml
    for dev in config['devices']:
        topic = f"v3/{config['ttn']['app_id']}@ttn/devices/{dev}/up"
        client.subscribe(topic, 0)


def on_subscribe(client, obj, mid, granted_qos):
    '''
    This runs once subscribed, and simply logs the result.
    '''
    mqtt_logger.info(f'Subscribed mid: {str(mid)}, qos: {str(granted_qos)}')


def on_message(client, obj, msg):
    '''
    Function to process the message payload and execute 'insert_row' on each
    message received.
    '''
    # get the message
    message = json.loads(msg.payload)
    # get the values from the payload
    raw_values = message['uplink_message']['decoded_payload']
    # if there is data, insert it to the device's table
    if len(raw_values) > 0:
        # flatten the values dict, CayenneLPP nests them for some payload types
        values = flatten_dict(raw_values)
        # get associated device id
        device_id = message['end_device_ids']['device_id']
        # and insert
        insert_row(values, device_id)

###############################################################################


###############################################################################
#              Client initialisation for paho-mqtt lives here                 #
###############################################################################


def client_init():
    '''
    Function to create callbacks for operatios above, then connect to the
    client with the username and password specified in the config file.
    '''
    # create a client
    client = mqtt.Client()
    # create callbacks from functions above
    client.on_connect = on_connect
    client.on_subscribe = on_subscribe
    client.on_message = on_message
    # set logging level
    client.enable_logger(mqtt_logger)
    # set up connection
    client.tls_set()
    client.username_pw_set(f"{config['ttn']['app_id']}@ttn",
                           config['ttn']['password'])
    # connect
    client.connect(config['ttn']['server'], config['ttn']['port'], 60)

    return client

###############################################################################


###############################################################################
#       This is where we grab our config and create each device table         #
#       Everything in here is ran at runtime.                                 #
###############################################################################


# get our args
args = parse_args(DESCRIPTION, EXAMPLE)

# set log level and format
basicConfig(level=args.logging,
            format='%(asctime)s %(levelname)-8s %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S',
            )

# create some logging objects
run_logger = getLogger('RUNTIME')
mqtt_logger = getLogger('MQTT')
db_logger = getLogger('DB')

# get our config from the file specified in the args
try:
    config = get_yaml(args.config)
except Exception as e:
    run_logger.error(e)
    exit(-1)

# get postgresql config and paramters
pg_user = config['postgres']['username']
pg_pass = config['postgres']['password']
pg_host = config['postgres']['host']
pg_port = config['postgres']['port']
pg_dbase = config['postgres']['database']
# construct url from above
pg_url = f"postgresql://{pg_user}:{pg_pass}@{pg_host}:{pg_port}/{pg_dbase}"

# create an engine for the postgresql connection
engine = create_engine(pg_url)

# are we doing hypertables?
hypterables = config['postgres']['hypertables']
# create a table for each device containing it's specific columns
device_tables = {}
for dev in config['devices']:
    cols = config['devices'][dev]['columns']
    device_tables[dev] = build_table(dev, cols, hypertables=hypterables)
    device_tables[dev].metadata.create_all(engine)

###############################################################################

if __name__ == '__main__':
    # connect to TTN
    client = client_init()
    # loop over everything
    client.loop_forever()
